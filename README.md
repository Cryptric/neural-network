# Neural Network with Java
This repository contains a neural network library for supervised machine learning written in Java . The compiled library can be downloaded [here](https://gitlab.com/Cryptric/neural-network/-/releases/v1.0).

This repository contains the source code of a fully functional neural network. The code was part of a school project and is narrowed down to the basic function of a neural network for supervised learning.

# Usage

## Create a neural network
With this library neural networks with multiple layers can be created. All those layers are fully conacted layers. The layer count and the amount of neurons are passed to the constructer using a double array.
```java
NeuralNetwork nn = new NeuralNetwork(new int[] {784, 100, 10, 10});
```
This code creates a new network with an input layer with 784 neurons, two hidden layers with 100 and 10 neurons and an output layer with 10 neurons.

## Initialisation
After a network is created all values should be initialised randamly. To the <code>randomize</code> function does that. As an optional parameter a seed can be passed in this way all values will always be the same for the same seed.
```java
nn.randomize();
// or with seed
nn.randomize(76721);
```
## Learning rate
The default learning rate is set to 0.1. To change ist use the <code>setLearningRate</code> function and pass the new learning rate as an argument. The learning rate should always be a positive number.
```java
nn.setLearningRate(0.02);
```

## Predictions
To get a prediction from the network the <code>predict</code> function can be used. The inputs must be passed as an double array. The return value will be the activation of the last layer as a double array. The return values will always be between 0 and 1.
```java
double[] result = nn.predict(inputData);
```
## Training
To train the network the <code>train</code> function can be used. Inputs and the desired outputs for the training sample have to be passed to the function both as double arrays. This function will perform one learning step on the network using the backpropagation algorithm. To get good results many training steps with randomly sorted training data have to be performed. The function is going to return the error for this training sample.
```java
double error = nn.train(inputData, desiredOutputs);
```

## Example
The following code trains a neural network on the mnist data set. The data is read from a csv file downloadable [here](https://www.kaggle.com/oddrationale/mnist-in-csv). The network only trains for about half a minute and still reaches a precision of about 90%. To increase performance the learning rate can be changed and more training loops can be performed.
```java
package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

import net.NeuralNetwork;

public class Main {
	
	
	private static LinkedList<double[]> inputData = new LinkedList<>();
	private static LinkedList<double[]> desiredOutputData = new LinkedList<>();

	public static void main(String[] args) {

		/* =========== READ DATA =========== */
		System.out.println("Start reading data");
		try {
			readCSV("mnist_train.csv");
		} catch (IOException e) {
			System.err.println("could not read csv data");
			e.printStackTrace();
		}
		
		/* =========== START ACTUAL NEURAL NETWORK CODE =========== */
		NeuralNetwork nn = new NeuralNetwork(new int[] {784, 100, 100, 10}); // create neural network
		
		nn.randomize(); // initialise all values
		
		nn.setLearningRate(0.02); // change the learning rate
		
		// train neural network
		System.out.println("Start training");
		for (int i = 0; i < 1000 * 10; i++) { // training loop
			int r = (int)(Math.random() * inputData.size()); // select random training sample
			nn.train(inputData.get(r), desiredOutputData.get(r)); // perform training step
			if(i % 1000 == 0) {
				System.out.println("Training cycle " + i);
			}
		}
		
		// check performance
		int correctAnswers = 0;
		for (int i = 0; i < 1000; i++) { // test loop
			int r = (int)(Math.random() * inputData.size()); // select random testing sample
			double[] nnAnswer = createLabel(getMaxIndex(nn.predict(inputData.get(r)))); // get network answer and convert it to a double array only containing zeros and a 1 at the highest value
			double[] nnCorrectAnswer = desiredOutputData.get(r); // answer from the training set
			if(Arrays.equals(nnAnswer, nnCorrectAnswer)) { // if the results are equal add one the the correct counter
				correctAnswers++;
			}
		}
		
		System.out.println(correctAnswers + " from: 1000 correct, " + ((correctAnswers / 1000.0) * 100) + "%"); // print the networks performance
		
	}
	
	/**
	 * reads data from csv file
	 * @param aPath path to csv file
	 */
	private static void readCSV(String aPath) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(aPath));
		
		reader.readLine(); // skip header line
		
		String line;
		while ((line = reader.readLine()) != null) { // read line by line
			String[] stringValues = line.split(","); // split columns
			
			// create label from target number
			desiredOutputData.add(createLabel(Integer.parseInt(stringValues[0]))); 
			
			// create input data
			double[] data = new double[784]; // array for input values
			for(int i = 1; i < stringValues.length; i++) { // start by one because first value is label
				data[i-1] = Double.parseDouble(stringValues[i]) / 255.0; // scale the values down to a range between 0 and 1
			}
			inputData.add(data);
			
		}
		reader.close();
	}
	
	/**
	 * creates label from number to double array
	 * @param aNumber label number
	 * @return label double array, corresponding number is 1 all others are 0
	 */
	private static double[] createLabel(int aNumber) {
		double[] label = new double[10];
		for(int i = 0; i < 10; i++) {
			if(i == aNumber) {
				label[i] = 1;
			} else {
				label[i] = 0;
			}
		}
		return label;
	}
	
	/**
	 * returns the max values index
	 * @param aArray array
	 * @return the max values index
	 */
	private static int getMaxIndex(double[] aArray) {
		double max = Double.MIN_VALUE;
		int index = -1;
		for (int i = 0; i < aArray.length; i++) {
			if(aArray[i] > max) {
				max = aArray[i];
				index = i;
			}
		}
		return index;
	}

}
```