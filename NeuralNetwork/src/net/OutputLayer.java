package net;

import java.io.Serializable;

public class OutputLayer extends Layer implements Cloneable, Serializable {

    /**
     * Creates a new layer for a net, which includes the weights / biases w(i, j) for the layer "j"
     * @param aNumberNodes number of nodes for this layer
     * @param aNumberPrevNodes number of nodes of the previous layer
     */
    public OutputLayer(int aNumberNodes, int aNumberPrevNodes) {
        super(aNumberNodes, aNumberPrevNodes);
    }

}