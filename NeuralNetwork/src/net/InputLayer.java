package net;

import math.Matrix;

import java.io.Serializable;

public class InputLayer extends Layer implements Cloneable, Serializable {
    /**
     * Creates a new input layer for a net, doesn't apply any activation function, doesn't do any calculation, just passes the input values
     * @param aNumberInputNodes
     */
    public InputLayer(int aNumberInputNodes) {
        super(aNumberInputNodes);
    }

    /**
     * does nothing because it is an input layer
     */
    @Override
    public void randomize() {

    }

    /**
     * does nothing because it is an input layer
     */
    @Override
    public void randomize(long aSeed) {

    }

    /**
     * just passes the inputs
     * @param aPrevActivation the network inputs
     * @return the inputs
     */
    @Override
    public Matrix feedForward(Matrix aPrevActivation) {
        return aPrevActivation;
    }

    /**
     * does nothing because it is an input layer
     * @param aError the previous layers error
     * @param aActivationThisLayer this layers activation from the feed forward
     * @param aActivationPrevLayer the previous layers activation from the feed forward
     * @param aLearningRate the error from this layer
     * @return null
     */
    @Override
    public Matrix train(Matrix aError, Matrix aActivationThisLayer, Matrix aActivationPrevLayer, double aLearningRate) {
        return null;
    }

}

