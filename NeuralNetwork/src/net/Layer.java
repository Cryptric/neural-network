package net;

import math.Matrix;

import java.io.Serializable;

public abstract class Layer implements Cloneable, Serializable {

    /*  Matrix[row][column] = [nodes in this layer][nodes in prev layer]
     *
     *   W(1,1) W(2,1)
     *   W(1,2) W(2,2)
     * */
    private Matrix mWeights;

    /* Matrix[row][column] = [nodes][1]*/
    private Matrix mBiases;

    private int mNumberNodes;

    /**
     * Creates a new layer for a net, which includes the weights / biases w(i, j) for the layer "j"
     * @param aNumberNodes number of nodes for this layer
     * @param aNumberPrevNodes number of nodes of the previous layer
     */
    public Layer(int aNumberNodes, int aNumberPrevNodes) {
        mNumberNodes = aNumberNodes;
        mWeights = new Matrix(mNumberNodes, aNumberPrevNodes);
        mBiases = new Matrix(mNumberNodes, 1);

    }

    /**
     * Creates a new layer for a net, which includes the weights / biases w(i, j) for the layer "j"
     * @param aNumberNodes number of nodes for this layer
     */
    public Layer(int aNumberNodes) {
        mNumberNodes = aNumberNodes;
    }

    /**
     * initializes all weights and biases randomly
     */
    public void randomize() {
        mWeights.randomize();
        mBiases.randomize();
    }

    /**
     * initializes all weights and biases randomly using a seed
     * @param aSeed seed
     */
    public void randomize(long aSeed) {
        mWeights.randomize(aSeed);
        mBiases.randomize(aSeed);
    }

    /**
     * calculates the activation for this layer using the previous layer activation
     * @param aPrevActivation previous layer activation
     * @return this layer activation
     */
    public Matrix feedForward(Matrix aPrevActivation) {
        Matrix result = Matrix.multiply(mWeights, aPrevActivation); //Matrix[row][column] = [nodes in this layer][1]
        result.add(mBiases);
        result.applySigmoid();
        return result;
    }

    /**
     * performs a learning step on this layers weights and biases using backpropagation
     * @param aError the previous layers error
     * @param aActivationThisLayer this layers activation from the feed forward
     * @param aActivationPrevLayer the previous layers activation from the feed forward
     * @param aLearningRate the error from this layer
     * @return
     */
    public Matrix train(Matrix aError, Matrix aActivationThisLayer, Matrix aActivationPrevLayer, double aLearningRate) {

        Matrix activationThisLayer = new Matrix(aActivationThisLayer);
        activationThisLayer.applyDSigmoidToSigmoid();

        Matrix activationPrevLayer = new Matrix(aActivationPrevLayer);
        activationPrevLayer.transpose();

        Matrix m1 = new Matrix(aError);
        m1.scale(-1);
        m1.multiply(activationThisLayer);

        Matrix dW = Matrix.multiply(m1, activationPrevLayer);
        dW.scale(aLearningRate);
        mWeights.add(dW);

        m1.scale(aLearningRate);
        mBiases.add(m1);

        Matrix m2 = new Matrix(mWeights);
        m2.transpose();
        return Matrix.multiply(m2, aError);
    }

}
