package net;

import math.Matrix;

import java.io.*;

public class NeuralNetwork implements Cloneable, Serializable {

    /* includes input and output layers*/
    private int mNumberLayers;

    private Layer[] mLayers;

    private double mLearningRate = -0.1;

    /**
     * Creates a new neural network
     * input layer counts as layer but doesn't do any calculation -> neural network with 3 layers -> has 2 sets of weights and biases
     * @param aLayerNodesCount neurons per layer inclusive input and output layer, every array element is one layer with an accordingly large number of neurons 
     */
    public NeuralNetwork(int[] aLayerNodesCount) {
        mNumberLayers = aLayerNodesCount.length;
        if (mNumberLayers <= 1) {
            throw new IllegalArgumentException("Cant create a neural network with 1 layer");
        }

        mLayers = new Layer[mNumberLayers];

        for (int i = 0; i < mNumberLayers; i++) {
            if (i == 0) {
                mLayers[i] = new InputLayer(aLayerNodesCount[i]);
            } else if (i == mNumberLayers - 1) {
                mLayers[i] = new OutputLayer(aLayerNodesCount[i], aLayerNodesCount[i - 1]);
            } else {
                mLayers[i] = new HiddenLayer(aLayerNodesCount[i], aLayerNodesCount[i - 1]);
            }
        }
    }

    /**
     * initializes all weights and biases randomly
     */
    public void randomize() {
        for (Layer l : mLayers) {
            l.randomize();
        }
    }

    /**
     * initializes all weights and biases randomly using a seed
     * @param aSeed seed
     */
    public void randomize(long aSeed) {
        for (Layer l : mLayers) {
            l.randomize(aSeed);
        }
    }
    
    /**
     * calculates a network prediction
     * @param aInputs neural network inputs as double array
     * @return the last layers activation as double array, all values are between 0 and 1
     */
    public double[] predict(double[] aInputs) {
        Matrix activation = new Matrix(aInputs);
        for (int i = 0; i < mNumberLayers; i++) {
            activation = mLayers[i].feedForward(activation);
        }
        return activation.toSingleArray();
    }

    /**
     * Trains the net with the given training sample element
     * @param aInputs the inputs for the net
     * @param aTargets the desired outputs
     * @return the net error for this training sample
     */
    public double train(double[] aInputs, double[] aTargets) {
        double loss;
        Matrix[] activations = new Matrix[mNumberLayers];
        for (int i = 0; i < mNumberLayers; i++) {
            if (i == 0) {
                activations[i] = mLayers[i].feedForward(new Matrix(aInputs));
            } else {
                activations[i] = mLayers[i].feedForward(activations[i - 1]);
            }
        }

        Matrix error = new Matrix(aTargets);
        error.subtract(activations[mNumberLayers - 1]);

        Matrix temp1 = new Matrix(error);
        temp1.transpose();
        loss = Matrix.multiply(temp1, error).getData()[0][0];

        for (int i = 1; i < mNumberLayers; i++) { // i = 1 -> because it shouldn't reach the input layer
            error = mLayers[mNumberLayers - i].train(error, activations[mNumberLayers - i], activations[mNumberLayers - i - 1], mLearningRate);
        }

        return loss;
    }
    
    /**
     * changes the default learning rate
     * @param aValue the new learning rate, should always be a positive number
     */
    public void setLearningRate(double aValue) {
    	mLearningRate = -aValue;
    }

    /**
     * Clones the neural network
     * @return the cloned network
     */
    public NeuralNetwork clone() throws CloneNotSupportedException {
        return (NeuralNetwork) super.clone();
    }


    public void saveNeuralNetwork(String path) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(path);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this);
        out.close();
        fileOut.close();
    }


    public static NeuralNetwork loadNeuralNetwork(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(path);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        NeuralNetwork nn = (NeuralNetwork) in.readObject();

        return nn;
    }


}
