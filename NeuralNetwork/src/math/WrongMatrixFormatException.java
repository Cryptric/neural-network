package math;

public class WrongMatrixFormatException extends IllegalArgumentException {

    /**
     * throw this exception if two incompatible matrices should be calculated due to wrong row and column counts
     * @param aM1 operator 1
     * @param aM2 operator 2
     * @param aOperation operation type
     */
    public WrongMatrixFormatException(Matrix aM1, Matrix aM2, String aOperation) {
        super("Can't " + aOperation + " Matrix 1 with " + aM1.getNumberRows() + " rows and " + aM1.getNumberCols() + " columns & Matrix 2 with " + aM2.getNumberRows() + " rows and " + aM2.getNumberCols() + " columns!");
    }

    /**
     * throw this exception if two incompatible matrices should be calculated due to wrong row and column counts
     * @param aMsg error msg
     */
    public WrongMatrixFormatException(String aMsg) {
        super(aMsg);
    }

}
