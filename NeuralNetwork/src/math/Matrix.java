package math;

import java.io.Serializable;
import java.lang.Math;
import java.util.Random;

public class Matrix implements Cloneable, Serializable {

    private int mNumberRows;
    private int mNumberCols;

    /*  mData [row][col] */
    private double[][] mData;

    /**
     * creates a new matrix
     * all values are initialized with 0
     * @param aNumberRows number of rows
     * @param aNumberCols number of columns
     */
    public Matrix(int aNumberRows, int aNumberCols) {
        mNumberRows = aNumberRows;
        mNumberCols = aNumberCols;
        mData = new double[mNumberRows][mNumberCols];
    }

    /**
     * creates a new matrix with the given values
     * @param aData data for the matrix
     */
    public Matrix(double[][] aData) {
        mNumberRows = aData.length;
        mNumberCols = aData[0].length;
        mData = new double[mNumberRows][mNumberCols];

        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = aData[i][j];
            }
        }
    }

    /**
     * Creates a one column matrix from a double array
     * @param aData data for the matrix
     */
    public Matrix(double[] aData) {
        mNumberRows = aData.length;
        mNumberCols = 1;
        mData = new double[mNumberRows][mNumberCols];
        for (int i = 0; i < mNumberRows; i++) {
            mData[i][0] = aData[i];
        }
    }

    /**
     * Clones the given matrix
     * @param aMatrix source matrix
     */
    public Matrix(Matrix aMatrix) {
        this(aMatrix.getData());
    }

    /**
     * sets all matrix values to a random double between 0 and 1
     * using pseudo random with a seed
     * @param aSeed seed for the random numbers
     */
    public void randomize(long aSeed) {
        Random rand = new Random(aSeed);
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = rand.nextDouble();
            }
        }
    }

    /**
     * sets all matrix values to a random double between -1 and 1
     */
    public void randomize() {
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = (Math.random() * 2) - 1;
            }
        }
    }

    /**
     * sets all matrix values to a random double between min and max
     * @param aMin random minimum number
     * @param aMax random maximum number
     */
    public void randomize(double aMin, double aMax) {
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = math.Math.randomBetween(aMin, aMax);
            }
        }
    }

    /**
     * scales the matrix
     * @param aValue factor
     */
    public void scale(double aValue) {
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] *= aValue;
            }
        }
    }

    /**
     * adds the given matrix
     * @param aMatrix addend
     */
    public void add(Matrix aMatrix) {
        if (mNumberRows != aMatrix.getNumberRows() || mNumberCols != aMatrix.getNumberCols()) {
            throw new WrongMatrixFormatException(this, aMatrix, "add");
        }
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] += aMatrix.getData()[i][j];
            }
        }
    }

    /**
     * subtracts the given matrix from the base matrix
     * @param aMatrix subtrahend
     */
    public void subtract(Matrix aMatrix) {
        if (mNumberRows != aMatrix.getNumberRows() || mNumberCols != aMatrix.getNumberCols()) {
            throw new WrongMatrixFormatException(this, aMatrix, "subtract");
        }
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] -= aMatrix.getData()[i][j];
            }
        }
    }

    /**
     * Element wise multiplication
     * @param aMatrix multiplier
     */
    public void multiply(Matrix aMatrix) {
        if (mNumberRows != aMatrix.getNumberRows() || mNumberCols != aMatrix.getNumberCols()) {
            throw new WrongMatrixFormatException(this, aMatrix, "multiply");
        }
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] *= aMatrix.getData()[i][j];
            }
        }
    }

    /**
     * applies sigmoid to all matrix values
     */
    public void applySigmoid() {
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = math.Math.sigmoid(mData[i][j]);
            }
        }
    }

    /**
     * Applies the DSigmoid function to the matrix that was already applied with the sigmoid function
     */
    public void applyDSigmoidToSigmoid() {
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                mData[i][j] = math.Math.dSigmoidForSigmoid(mData[i][j]);
            }
        }
    }

    /**
     * Transforms a matrix with one column to a double array
     * @return matrix as double array
     */
    public double[] toSingleArray() {
        if (mNumberCols > 1) {
            throw new WrongMatrixFormatException("Cant create 1d array from Matrix with " + mNumberRows + " rows and " + mNumberCols + "columns!");
        }
        double[] array = new double[mNumberRows];
        for (int i = 0; i < mNumberRows; i++) {
            array[i] = mData[i][0];
        }
        return array;
    }

    /**
     * transposes the matrix
     */
    public void transpose() {
        double[][] dataTransposed = new double[mNumberCols][mNumberRows];
        for (int i = 0; i < dataTransposed.length; i++) {
            for (int j = 0; j < dataTransposed[i].length; j++) {
                dataTransposed[i][j] = mData[j][i];
            }
        }
        mData = dataTransposed;
        mNumberCols = mNumberRows;
        mNumberRows = mData.length;
    }

    /**
     * Multiplies the to matrices using matrix multiplication and returns the result in a new matrix
     * @param aM1 Matrix 1
     * @param aM2 Matrix 2
     * @return result from matrix multiplication
     */
    public static Matrix multiply(Matrix aM1, Matrix aM2) {
        if (aM1.getNumberCols() != aM2.getNumberRows()) {
            throw new WrongMatrixFormatException(aM1, aM2, "dot multiply");
        }
        Matrix result = new Matrix(aM1.getNumberRows(), aM2.getNumberCols());
        for (int i = 0; i < result.getNumberRows(); i++) {          // 1 2
            for (int j = 0; j < result.getNumberCols(); j++) {      // 3 4
                double sum = 0;
                for (int k = 0; k < aM1.getNumberCols(); k++) {
                    sum += aM1.getData()[i][k] * aM2.getData()[k][j];
                }
                result.getData()[i][j] = sum;
            }
        }
        return result;
    }


    public int getNumberRows() {
        return mNumberRows;
    }

    public int getNumberCols() {
        return mNumberCols;
    }

    public double[][] getData() {
        return mData;
    }
    
    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < mNumberRows; i++) {
            for (int j = 0; j < mNumberCols; j++) {
                s += mData[i][j] + "\t";
            }
            s += "\n";
        }
        return s;
    }

}
