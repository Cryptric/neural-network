package math;

public class Math {

    /**
     * applies the sigmoid function to the given value
     * @param aX input value
     * @return result from sigmoid
     */
    public static double sigmoid(double aX) {
        return 1 / (1 + java.lang.Math.exp(-aX));
    }

    /**
     * applies the derived sigmoid function to the given sigmoid value
     * @param aX input value sigmoid must already be applied to it
     * @return result from derived sigmoid function
     */
    public static double dSigmoidForSigmoid(double aX) {
        return (aX * (1 - aX));
    }

    /**
     * creates a random number between min and max
     * @param aMin minimal value
     * @param aMax maximal value
     * @return random double between aMin and aMax
     */
    public static double randomBetween(double aMin, double aMax) {
        if (aMin >= aMax) {
            System.err.println("Min must be bigger than max");
        }
        double rand = java.lang.Math.random();
        rand *= (aMax - aMin);
        rand += aMin;
        return rand;
    }

}